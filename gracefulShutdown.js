const death = require('death');

module.exports = function createMiddleware(server, opts) {

  let shuttingDown = false;

  const options = Object.assign({}, {
    logger: console,
    forceTimeout: 30000
  }, opts);

  death(gracefulExit);

  function gracefulExit() {
    if (shuttingDown) {
      return;
    }

    shuttingDown = true;
    options.logger.warn('Received kill signal (SIGTERM), shutting down');

    setTimeout(() => {
      options.logger.error('Could not close connections in time, forcefully shutting down');
      process.exit(1)
    }, options.forceTimeout);

    server.close(() => {
      options.logger.info('Closed out remaining connections.');
      process.exit()
    });
  }

  function middleware(req, res, next) {
    if (!shuttingDown) {
      return next();
    }

    res.set('Connection', 'close');
    res.status(503).send('Server is in the process of restarting.');
  }

  return middleware;
};
